import datetime


class Sprint(object):

    version = 1.0

    def __init__(self, name):
        self.name = name
        self._tasks = []

    def add_task(self, task):
        self._tasks.append(task)
        task.set_sprint(self)

    def remove_task(self, task):
        if task in self._tasks:
            self._tasks.pop(self._tasks.index(task))

    def _snapshot(self):
        status = {
            "ontrack":[],
            "delayed":[]
        }
        now = datetime.datetime.now().date()
        for t in self._tasks:
            if now > t.due_date:
                status["delayed"].append(t)
            else:
                status["ontrack"].append(t)

        return status

    def display_staus(self):
        snapshot = self._snapshot()
        for (k, v) in snapshot.items():
            print(k)
            for t in v:
                print ("\t{}".format(t.title))
