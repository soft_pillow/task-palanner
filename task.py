
class Task(object):

    def __init__(self, name, creator, task_type, due_date, assigne=None):

        self.title = name
        self.creator = creator
        self.assignee = assigne
        self.type = task_type
        self.due_date = due_date
        self._sprint = None
        self._status = None
        self.creator.add_task(self)

    def set_sprint(self, sprint):
        self._sprint = sprint

    @property
    def sprint(self):
        return self._sprint

    def set_status(self, status):
        if status in self.STATUS:
            self._status = status.name

    @property
    def status(self):
        return self._status