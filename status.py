from enum import Enum

class Status(Enum):
    open = 1
    in_progress = 2
    testing = 3
    deployed = 4
    fixed = 5
    compleated = 6