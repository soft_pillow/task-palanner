from SprintPlanner.status import Status
from SprintPlanner.task import Task


class SubTask(object):

    STATUS = [Status.open, Status.in_progress, Status.compleated]

    def __init__(self, title, status):
        self.title = title
        self.status = status


class Story(Task):

    STATUS = [Status.open, Status.in_progress, Status.compleated]
    sub_tasks = []

    def __init__(self, name, creator, due_date, summary, assigne=None):
        self.summary = summary
        super(Story, self).__init__(name, creator, "STORY", due_date, assigne)

    def add_subtask(self, sub):
        if self.status != Status.compleated:
            self.sub_tasks.append(sub)