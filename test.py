from SprintPlanner import *
import datetime

sprint = Sprint("NovHackFest")

guru = User("gururaj")
bob = User("bob")
amy = User("amy")

b1= Bug("fix mysql",guru,datetime.date(2019,10,30),"P0")

f1 = Feature("create dashboard", bob, datetime.date(2019,11,30),"build a new dashboard", "high", "melvin")

s1 = Story("create a microservice", amy, datetime.date(2019,11,30), "summary to be updated......")

st1 = SubTask("unit test", amy)
s1.add_subtask(st1)

sprint.add_task(f1)
sprint.add_task(b1)
sprint.add_task(s1)