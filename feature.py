from SprintPlanner.status import Status
from SprintPlanner.task import Task


class Feature(Task):

    STATUS = [Status.open, Status.in_progress, Status.testing, Status.deployed]

    def __init__(self, name, creator, due_date, summary, impact, assigne=None):
        self.summary = summary
        self.impact = impact
        super(Feature, self).__init__(name, creator, "FEATURE", due_date, assigne)

