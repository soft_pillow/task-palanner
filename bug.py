from SprintPlanner.task import Task
from SprintPlanner.status import Status


class Bug(Task):

    STATUS = [Status.open, Status.in_progress, Status.compleated]

    def __init__(self, name, creator, due_date, severity, assigne=None):
        self.severity = severity
        super(Bug, self).__init__(name, creator, "BUG", due_date, assigne)
